//admin color
function durham_admin_color_scheme() {
	//Get the theme directory
	$theme_dir = get_stylesheet_directory_uri();
  
	//Durham
	wp_admin_css_color( 'durham', __( 'Durham' ),
	  $theme_dir . '/durham_admin.css',
	  array( '#68246d', '#fff', '#406e49' , '#8f00aa')
	);
  }
  add_action('admin_init', 'durham_admin_color_scheme');